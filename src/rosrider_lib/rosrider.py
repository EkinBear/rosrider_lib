#!/usr/bin/env python

import rospy
import math

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from std_srvs.srv import Empty
from nav_msgs.msg import Odometry
from rosrider_description.srv import CameraSensor, RosriderControl

from tf.transformations import quaternion_from_euler, euler_from_quaternion


class ROSRider:

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []

        def convert_str_to_rgb(self, data):
            self.data = []

            for i in range(self.height * self.width * 3):
                self.data.append(ord(data[i]))

    # init method. image_cb is camera callback function
    def __init__(self, robotName, rate):

        self.robotName = robotName
        self.is_shutdown = False

        self.x = 0.0
        self.y = 0.0
        self.yaw = 0.0
        self.vel_x = 0.0
        self.vel_z = 0.0

        self.image = self._Image()

        self.lx = 0
        self.az = 0
        self.step_size = 10

        rospy.init_node(self.robotName)
        rospy.loginfo("{0} started".format(self.robotName))

        # shutdown handler
        def shutdown():
            self.is_shutdown = True
            rospy.loginfo('shutdown robot')

        rospy.on_shutdown(shutdown)

        self.r = rospy.Rate(rate)

        self.sensor_status = self._init_sensor_services()
        self.control_status = self._init_rosrider_control()
        self.pause_status = self._pause_physics()

    def move(self, linear):
        if not self.is_shutdown:
            self.lx = linear
            self.control(self.lx, self.az, self.step_size)

    def rotate(self, angular):
        if not self.is_shutdown:
            self.az = angular
            self.control(self.lx, self.az, self.step_size)

    def stop(self):
        self.lx = 0
        self.az = 0
        self.control(self.lx, self.az, self.step_size)

    def sleep(self):
        self.r.sleep()

    def get_image(self):
        resp = self.image_data_service()

        self.image.data = resp.data

        return self.image

    def is_ok(self):
        self.pause_status = self._pause_physics()
        if self.sensor_status and self.control_status and self.pause_status:
            if not rospy.is_shutdown():
                return True
        return False

    
    def _pause_physics(self):
        try:
            rospy.wait_for_service("/gazebo/pause_physics")

            self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)

            resp = self.pause()
            print(resp)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/front_camera/rosrider_get_image", 0.5)

            self.image_data_service = rospy.ServiceProxy('/front_camera/rosrider_get_image', CameraSensor)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            self.image.convert_str_to_rgb(resp.data)

            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def _init_rosrider_control (self):
        try:
            rospy.wait_for_service("/rosrider_control", 0.5)

            self.control = rospy.ServiceProxy('/rosrider_control', RosriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False
